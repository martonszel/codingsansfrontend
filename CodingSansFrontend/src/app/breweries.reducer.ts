import { BreweriesActionTypes, BreweriesActions } from './breweries.action';
import { Brewerie } from './models/breweries';
import { AppState, initialState } from './breweries.state';

export function BreweriesReducer(
                state = initialState,
                action: BreweriesActions) {
  switch (action.type) {
    case BreweriesActionTypes.LOAD_BREWERIES_SUCCESS:
      return {
        ...state,
         breweries: action.payload};
    case BreweriesActionTypes.SELECT_BREWERIES:
      return {
        ...state,
        selectedBrewerie: action.payload};
    default:
      return state;
  }}
