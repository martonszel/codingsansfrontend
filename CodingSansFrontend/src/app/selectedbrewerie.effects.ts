import { Injectable } from '@angular/core';
import { BreweriesService } from './breweries.service';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import {  SelectBrewerieAction, BreweriesActionTypes   } from './breweries.action';

import { Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SelectedBrewerieEffects {
  constructor(private actions$: Actions,
              private router: Router,
  ) {

    this.actions$.pipe(
      ofType(BreweriesActionTypes.SELECT_BREWERIES),
      map( r => r as SelectBrewerieAction))
      .subscribe( r => {
        this.router.navigate(['/breweries/' + r.payload.id ]);
      });
  }
  }
