import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BreweriesComponent } from './breweries/breweries.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BreweriesReducer } from './breweries.reducer';
import { BreweriesEffects } from './breweries.effects';
import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SelectedbrewerieComponent } from './selectedbrewerie/selectedbrewerie.component';
import { SelectedBrewerieEffects } from './selectedbrewerie.effects';
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    AppComponent,
    BreweriesComponent,
    SelectedbrewerieComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatButtonModule,
    StoreDevtoolsModule.instrument({
      name: 'coding-sans-frontend',
      maxAge: 25, // Retains last 25 states
    }),
    BrowserAnimationsModule,
    StoreModule.forRoot({
      applicationState: BreweriesReducer
    }),
    EffectsModule.forRoot([BreweriesEffects, SelectedBrewerieEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
