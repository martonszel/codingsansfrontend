import { Action } from '@ngrx/store';
import { Brewerie } from './models/breweries';

export enum BreweriesActionTypes {
  LOAD_BREWERIES = ' [Breweries] Load Breweries',
  LOAD_BREWERIES_SUCCESS = ' [Breweries] Load Breweries Success',
  LOAD_BREWERIES_FAIL = ' [Breweries] Load Breweries Fail ',
  SELECT_BREWERIES = ' [Breweries] Select Brewerie'
}

export class LoadBreweriesAction implements Action {
  readonly type = BreweriesActionTypes.LOAD_BREWERIES;
}

export class LoadBreweriesSuccessAction implements Action {
  readonly type = BreweriesActionTypes.LOAD_BREWERIES_SUCCESS;
  constructor(public readonly payload: Brewerie[]) {}
}

export class SelectBrewerieAction implements Action {
  readonly type = BreweriesActionTypes.SELECT_BREWERIES;
  constructor(public readonly payload: Brewerie) {}
}

export type BreweriesActions =
LoadBreweriesAction | LoadBreweriesSuccessAction |
SelectBrewerieAction ;

