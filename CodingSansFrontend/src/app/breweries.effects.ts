import { Injectable } from '@angular/core';
import { BreweriesService } from './breweries.service';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { BreweriesActionTypes, LoadBreweriesSuccessAction   } from './breweries.action';
import * as BreweriesActions from './breweries.action';
import { Brewerie } from './models/breweries';


@Injectable({
  providedIn: 'root'
})
export class BreweriesEffects {
  constructor(private breweriesService: BreweriesService,
              private actions$: Actions
  ) {}

  @Effect()
  loadBreweries$: Observable<Action> = this.actions$.pipe(
    ofType(BreweriesActionTypes.LOAD_BREWERIES),
    switchMap(() => this.breweriesService.getBreweries().pipe(
      map((breweries) => new LoadBreweriesSuccessAction(breweries))
    ))
  );
  }
