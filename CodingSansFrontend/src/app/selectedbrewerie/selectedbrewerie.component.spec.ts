import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedbrewerieComponent } from './selectedbrewerie.component';

describe('SelectedbrewerieComponent', () => {
  let component: SelectedbrewerieComponent;
  let fixture: ComponentFixture<SelectedbrewerieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedbrewerieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedbrewerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
