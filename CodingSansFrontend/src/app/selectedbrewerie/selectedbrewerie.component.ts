import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../breweries.state';
import { Observable } from 'rxjs';
import { Brewerie } from '../models/breweries';

@Component({
  selector: 'app-selectedbrewerie',
  templateUrl: './selectedbrewerie.component.html',
  styleUrls: ['./selectedbrewerie.component.scss']
})
export class SelectedbrewerieComponent implements OnInit {

  breweries$: Observable<any>;
  selectedBrewerie: Brewerie;

  constructor(private store: Store<AppState>) {
    this.breweries$ = this.store.select('applicationState');
   }

  ngOnInit() {
    this.breweries$.subscribe((state: AppState) =>
        this.selectedBrewerie = state.selectedBrewerie,
        );
}
}
