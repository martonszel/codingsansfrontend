import { Component, OnInit, AfterViewInit, ViewChild   } from '@angular/core';
import { Brewerie } from '../models/breweries';
import { Store } from '@ngrx/store';
import { AppState } from '../breweries.state';
import * as BreweriesActions from '../breweries.action';
import { Observable } from 'rxjs';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-breweries',
  templateUrl: './breweries.component.html',
  styleUrls: ['./breweries.component.scss']
})
export class BreweriesComponent implements OnInit, AfterViewInit  {

  public displayedColumns = ['id', 'name', 'select'];

  breweries$: Observable<any>;
  breweries: Brewerie[];
  dataSource = new MatTableDataSource<Brewerie>(this.breweries);

  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private store: Store<AppState>) {
    this.breweries$ = this.store.select('applicationState');
   }

  ngOnInit() {
    this.getBreweries();
    this.breweries$.subscribe((state: AppState) => this.breweries = state.breweries);
  }
  getBreweries() {
    this.store.dispatch(new BreweriesActions.LoadBreweriesAction());

  }
  navigatetoDetails(id: number) {
    const selectedbrewerie = this.breweries.find(b => b.id === id);
    this.store.dispatch(new BreweriesActions.SelectBrewerieAction(selectedbrewerie));
  }
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
}

/*
StoreModule.forRoot({blablabla: CustomerReducer}),


StoreModule.forRoot({applicationState: CustomerReducer}),
constructor(private store: Store<AppState>) {
  this.customers$ = this.store.select('applicationState');
}*/
