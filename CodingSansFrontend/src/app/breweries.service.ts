import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Brewerie } from './models/breweries';


@Injectable({ providedIn: 'root' })
export class BreweriesService {
  private baseUrl = 'https://api.openbrewerydb.org/breweries';
  constructor(private http: HttpClient) {}

  getBreweries(): Observable<Brewerie[]> {
    return this.http.get<Brewerie[]>(this.baseUrl)
      .pipe(
        tap(breweries => ({breweries})),
      );
  }
}
