import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BreweriesComponent } from './breweries/breweries.component';
import { SelectedbrewerieComponent } from './selectedbrewerie/selectedbrewerie.component';


const routes: Routes = [
  { path: '', redirectTo: '/breweries', pathMatch: 'full' },
  { path: 'breweries', component: BreweriesComponent },
  { path: 'breweries/:id',  component: SelectedbrewerieComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
