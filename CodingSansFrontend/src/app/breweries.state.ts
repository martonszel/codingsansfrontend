import { Brewerie } from './models/breweries';

export interface AppState {
   breweries: Brewerie[];
   selectedBrewerie?: Brewerie;
}

export const initialState: AppState = {breweries : [],
   selectedBrewerie: null};
