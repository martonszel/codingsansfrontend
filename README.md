# CodingSansFrontend

Simple app with [NGRX](https://ngrx.io/guide/store)

## Getting Started

- git clone (https://gitlab.com/martonszel/codingsansfrontend.git)
- cd codingsansfrontend/CodingSansFrontend

## Prerequisites

Step 1 
- Angular-CLI is hosted as a node_module and to install and use it you’ll need NodeJS and it’s package manager named npm / Node Package Manager /

- To install NodeJS, navigate to the Downloads Section of the NodeJS’ website, download and install the Installer, best supported by your OS.

Step 2 
- `npm install -g @angular/cli`

Step 3
- `npm i ` - To install the dependencies 

Step 4 
- `ng serve --open` - The ng serve command should start a development server on your locahost port 4200, so if you go to your browser and enter the following url: http://localhost:4200 

## Project 

- Brewery list & Brewery detail  
![Brewery](img/brewery.gif)


## Built With
- [Angular CLI](https://cli.angular.io/)
- [NGRX](https://ngrx.io/guide/store)
- [Angular material](https://material.angular.io/)
- ​[Open Brewery DB](https://www.openbrewerydb.org/#documentation)